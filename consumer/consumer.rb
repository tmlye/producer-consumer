#!/usr/bin/env ruby
require 'bunny'
require 'logger'
require 'yaml'
require_relative 'app/QueueClient.rb'
require_relative 'app/Transaction.rb'

client = QueueClient.new
client.connect

transactions = Array.new
account_balances = Hash.new(0)
logger = Logger.new('/proc/1/fd/1')

begin
  client.subscribe do |message|
    transaction = YAML::load(message)
    logger.info("Deserialized transaction: #{transaction}")
    transactions << transaction
    account_balances[transaction.account_id] += transaction.amount
    logger.info('New balances:')
    logger.info(account_balances)
  end
ensure
  client.close
  logger.info('Transactions in order:')
  logger.info(transactions)
  logger.info('Account balances:')
  logger.info(account_balances)
end
