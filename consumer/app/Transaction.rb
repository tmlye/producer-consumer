class Transaction
  attr_accessor :amount, :description, :account_id

  def initialize(amount, description, account_id)
    @amount = amount
    @description = description
    @account_id = account_id
  end

  def to_s
    "Amount: #{@amount}, Description: '#{@description}', AccountId: #{account_id}"
  end
end
