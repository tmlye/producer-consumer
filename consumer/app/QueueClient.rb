require 'logger'

class QueueClient
  def initialize
    @connection = Bunny.new(hostname: 'rabbitmq')
    @logger = Logger.new('/proc/1/fd/1')
  end

  def connect
    @connection.start
    @channel = @connection.create_channel
    @queue = @channel.queue('transactions')
    @logger.info("Connection established")
  rescue Bunny::TCPConnectionFailed
    sleep 2
    retry
  end

  def subscribe(&block)
    @queue.subscribe(block: true) do |_delivery_info, _properties, message|
      @logger.info("Received message #{message}")
      block.call(message)
    end
  end

  def close
    @logger.info("Closing connection")
    @connection.close
  end
end
