require 'logger'

class QueueClient
  def initialize
    @connection = Bunny.new(hostname: 'rabbitmq')
    @logger = Logger.new('/proc/1/fd/1')
  end

  def connect
    @connection.start
    @channel = @connection.create_channel
    @queue = @channel.queue('transactions')
    @logger.info("Connection established")
  rescue Bunny::TCPConnectionFailed
    sleep 2
    retry
  end

  def send(message)
    @channel.default_exchange.publish(message, routing_key: @queue.name)
    @logger.info("Sent '#{message.gsub("\n",' ')} to #{@queue.name}")
  end

  def close
    @logger.info("Closing connection")
    @connection.close
  end
end
