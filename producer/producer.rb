#!/usr/bin/env ruby
require 'bunny'
require 'logger'
require 'yaml'
require_relative 'app/QueueClient.rb'
require_relative 'app/Transaction.rb'

client = QueueClient.new
client.connect

begin
  account_ids = [ "0293842", "9845211", "5682734" ]
  loop do
    transaction = Transaction.new(
      rand(-1000..1000),
      'random transaction',
      account_ids[rand(0..2)]
    )
    client.send(YAML::dump(transaction))
    sleep rand(1..10)
  end
ensure
  client.close
end
